# !/bin/bash

sudo yum update -y
sudo amazon-linux-extras install -y docker
sudo systemctl enable docker
sudo systemctl start docker.service
sudo usermod -aG docker ec2-user